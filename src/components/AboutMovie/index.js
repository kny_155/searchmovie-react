import React from 'react';

import './style.css';

const AboutMovie = ({ props }) => {

    const {countries = [], data = 0, genres = []} = props;
    const readyCountries = countries.join(', ');
    const readyGenres = genres.join(', ');

    return (
        <div className="info-movie m-b">
            <ul>
                <li>Страна: <span className="info-text">{readyCountries}</span></li>
                <li>Год: <span className="info-text">{data} год</span></li>
                <li>Жанр: <span className="info-text">{readyGenres}</span></li>
            </ul>
        </div>
    );
};

export default AboutMovie;