import React, {Component} from 'react'

import "./style.css"

import Search from '../Search/'
import SearchList from "../SearchList";
import ViewListMovies from "../ViewListMovies";
import Movie from "../Movie";


export default class App extends Component{

    state = {
        movie: null,
        prevSearchText: "",
        showMovie: false,
        searchText: ""
    };




    onChangeSearch = (text) => {
        this.setState({
            searchText: text
        });
    };

    selectedMovie = (movie) => {
        this.setState({
            movie,
            prevSearchText: "",
            searchText: "",
        });
    };

    onSubmitSearch = () => {
        console.log("kek");
        this.setState({
            prevSearchText: this.state.searchText,
            movie: null
        })
    };

    render() {
        const {movie, prevSearchText, searchText} = this.state;
        const listMovies = prevSearchText ? <ViewListMovies movieName={prevSearchText} selectedMovie={this.selectedMovie}/> : null;
        const showMovie = movie && !prevSearchText ? <Movie movie={movie}/> : null;
        const isShowContent = listMovies || showMovie;
        const contentStyle = isShowContent ? "main-content" : "main-content_none";
        const styleSearch = isShowContent ? "search-place" : "search-place search-place_center";


        return (
            <div className="main-container">
                <div className={styleSearch}>
                    <div className="search-container">
                        <Search movieId={movie} onChangeSearch={this.onChangeSearch} onSubmitSearch={this.onSubmitSearch}/>
                        <SearchList searchText={searchText} selectedMovie={this.selectedMovie}/>
                    </div>
                </div>
                <div className={contentStyle}>
                    {listMovies}
                    {showMovie}
                </div>
            </div>
        )
    }
}