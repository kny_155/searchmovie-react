import React from 'react';

import './style.css'

const ImageMovie = ({url, openImage, index}) => {

    const baseUrl = `https://image.tmdb.org/t/p/w200`;
    return (
        <img className="image-movie" src={baseUrl + url} alt="Кадр из фильма" onClick={() => openImage(index)}/>
    )
};

export default ImageMovie;