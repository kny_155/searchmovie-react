import React from 'react';

import './style.css'

const InfoMovie = ({props}) => {

    const {budget = 0, revenue = 0, release = "", runtime = 0, rating = 0} = props;

    const noData = "Нет данных";
    const readyBudger = budget ? '$ ' + budget.toLocaleString() : noData;
    const readyRevenue = revenue ? '$ ' + revenue.toLocaleString() : noData;
    const readyRelease = release.split("-").reverse().join(".");

    return (
        <div className="info-movie">
            <ul>
                <li>Бюджет: <span className="info-text">{readyBudger}</span></li>
                <li>Сборы в мире: <span className="info-text">{readyRevenue}</span></li>
                <li>Премьера: <span className="info-text">{readyRelease}</span></li>
                <li>Время: <span className="info-text">{runtime || `-`} мин</span></li>
                <li>Рейтинг: <span className="info-text text-bold">{rating || `-`}</span></li>
            </ul>
        </div>
    );
};

export default InfoMovie;