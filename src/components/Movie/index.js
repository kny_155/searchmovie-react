import React, {Component} from 'react';
import MovieService from '../../service/theMovieDbService'

import './style.css'
import TitleMovie from "../TitleMovie";
import AboutMovie from "../AboutMovie";
import OverviewMovie from "../OverviewMovie";
import PosterMovie from "../PosterMovie";
import InfoMovie from "../InfoMovie";
import SliderImages from "../SliderImages";
import Spiner from "../Spiner";

export default class Movie extends Component {

    constructor(props) {
        super(props);
        this.getMovie(props.movie);
    }

    state = {
        movie: {},
    };

    movieService = new MovieService();



    getMovie = (id) => {
        this.movieService.getMovie(id)
            .then(movie => {
                    this.setState({
                        movie
                    })
                }
            )
    };

    componentDidUpdate(prevProps) {
        if(prevProps.movie !== this.props.movie){
            this.getMovie(this.props.movie);
        }

    }

    render() {
        const { name, originalName, countries,
            data, genres, overview, poster, budget,
            revenue, release, runtime, rating, images
        } = this.state.movie;



        const sliderImage = images && images.length ? <SliderImages images={images}/> : null;
        const loading = !name ? <Spiner/> : null;

        return (
            <div className="container">
                <div className="left-col">
                    <TitleMovie props={{name, originalName}} />
                    <AboutMovie props={{countries, data, genres}} />
                    <OverviewMovie overview={overview}/>
                    {sliderImage}
                </div>

                <div className="right-col">
                    <PosterMovie poster={poster} />
                    <InfoMovie
                        props={{budget, revenue, release, runtime, rating}}
                    />
                </div>
                {loading}
            </div>
        )
    }
}