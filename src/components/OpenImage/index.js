import React, {Component} from 'react';

import './style.css'

class OpenImage extends Component{

    state = {
        switchIndex: 0
    };

    baseUrl = `https://image.tmdb.org/t/p/original/`;

    nextImages = () => {
        if(this.state.switchIndex < this.props.images.length - 1){
            this.setState({
                switchIndex: this.state.switchIndex + 1
            });
        } else {
            this.setState({
                switchIndex: 0
            });
        }
    };

    pervImage = () => {
        if(this.state.switchIndex > 0){
            this.setState({
                switchIndex: this.state.switchIndex - 1
            });
        } else {
            this.setState({
                switchIndex: this.props.images.length - 1
            });
        }
    };

    closeImage = () => {
        this.setState({
            switchIndex: 0
        });
        this.props.closeImage();
    };

    componentWillMount() {
        this.setState({
            switchIndex: this.props.index
        });
    }

    render() {

        const {images} = this.props;
        const {switchIndex} = this.state;
        return (
            <div className="open-image" >
                <span className="open-image__close" onClick={this.closeImage}>&#10006;</span>
                <div className="open-image__content">
                    <span className="open-image__array" onClick={this.pervImage}>&#8249;</span>
                    <div className="open-image__image">
                        <img src={this.baseUrl + images[switchIndex]['url']} alt="Кадр из фильма" />
                    </div>
                    <span className="open-image__array" onClick={this.nextImages}>&#8250;</span>
                </div>
                <span className="open-image__count">{`${switchIndex + 1}/${images.length}`}</span>
            </div>

        )
    }

}

export default OpenImage;