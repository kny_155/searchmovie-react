import React from 'react';

import './style.css'

const OverviewMovie = ({ overview }) => {

    return (
        <div className="m-b">
            <p className="overview-movie">{overview}</p>
        </div>
    );
};

export default OverviewMovie