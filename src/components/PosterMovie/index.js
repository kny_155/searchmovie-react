import React from 'react';

import './style.css'

const PosterMovie = ({ poster = "" }) => {

    const url = poster ? "https://image.tmdb.org/t/p/w500": "";

    return (
        <div className="m-b">
            <img className="poster-movie" src={`${url}${poster}`} alt="Постер фильма" />
        </div>
    );
};

export default PosterMovie