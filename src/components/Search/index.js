import React, {Component} from 'react'

import './style.css'

export default class Search extends Component{

    style = {
        borderFocus: {
            borderBottomColor: '#311b92'
        }
    };

    state = {
        inputFocus: false,
        searchText: ""
    };

    onChangeSearch = (e) => {
        const text = e.currentTarget.value;
        this.setState({
            searchText: text
        });
        this.props.onChangeSearch(text);
    };

    onFocusSearch = () => {
        this.setState({
            inputFocus: true
        })
    };

    onBlurSearch = () => {
        this.setState({
            inputFocus: false
        })
    };

    onSubmitForm = (e) => {
        if(e){
            e.preventDefault();
        }
        this.setState({
            searchText: ""
        });
        this.props.onSubmitSearch();
        this.props.onChangeSearch('');
    };

    componentDidUpdate(prevProps){

        if(prevProps.movieId !== this.props.movieId ) { //!
            this.setState({
                searchText: ""
            });
        }
    }

    render() {
        const {borderFocus} = this.style;
        const {inputFocus, searchText} = this.state;
        const formStyle = inputFocus ? borderFocus : {};

        return (
            <form className="search"
                  style={formStyle}
                  onSubmit={this.onSubmitForm}
            >
                <input type="text"
                       className="search__input"
                       placeholder="Поиск"
                       value={searchText}
                       onChange={this.onChangeSearch}
                       onFocus={this.onFocusSearch}
                       onBlur={this.onBlurSearch}
                />
                <input type="submit" value="" className="search__search-img search__img"/>
            </form>
        )
    }
}