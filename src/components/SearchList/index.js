import React, {Component} from 'react'

import MovieService from '../../service/theMovieDbService'
import './style.css'
import SearchListElement from "../SearchListElement";

export default class SearchList extends Component{

    state = {
        movies: null,
        idTimer: null
    };

    movieService = new MovieService();

    searchMovies = (text) => {
        if(text.length > 2) {
            this.movieService.searchMovie(text)
                .then(({results: movies}) => {
                    if(text === this.props.searchText){
                        if(movies.length !== 0){
                            this.setState({
                                movies
                            })
                        } else {
                            this.setState({
                                movies: null
                            })
                        }
                    }
                });
        } else {
            this.setState({
                movies: null
            })
        }
    };

    componentWillUpdate(prevProps) {
        if(prevProps.searchText !== this.props.searchText){
            clearTimeout(this.state.idTimer);
            const idTimer = setTimeout(() => this.searchMovies(this.props.searchText), 300);
            this.setState({
                idTimer
            })
        }
    }

    selectedMovie = (movie) => {
        this.setState({
            movies: null
        });
        this.props.selectedMovie(movie);
    };


    createElement = (movies) => {
        if(movies){
           return movies.map((item) => (
               <SearchListElement key={item.id}
                                  item={item}
                                  selectedMovie={this.selectedMovie}
               />
           ));
        } else {
            return null;
        }
    };

    render() {
        const {movies} = this.state;
        const searchList = this.createElement(movies);
        const styleList = searchList ? "search-list" : "search-list_none";
        return (
            <div className={styleList}>
                {searchList}
            </div>
        )
    }

}

