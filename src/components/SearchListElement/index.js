import React, {Component} from 'react'

import './style.css'

class SearchListElement extends Component {


    render() {
        const {name, originalName, rating, releaseDate} = this.props.item;
        const vote = rating ? rating : "-";
        const original = originalName !== name ? " " + originalName : null;
        const date = releaseDate.slice(0, 4);
        const dataStyle = "search-list-elem__vote" + (rating ? " search-list-elem__vote_" + ( vote < 5 ? "low" : vote < 8 ? "medium" : "high" ) : "");


        return (
            <div className="search-list-elem" onClick={() => this.props.selectedMovie(this.props.item.id)}>
                <span className="search-list-elem__name">
                    <span className="search-list-elem__main-name">{name}</span>{original}, {date}
                </span>
                <div className={dataStyle}>{vote}</div>
            </div>
        )
    }

}

export default SearchListElement;