import React, {Component} from 'react';

import './style.css'
import ImageMovie from "../ImageMovie";
import OpenImage from "../OpenImage";

class SliderImages extends Component {

    state = {
        openImageId: -1
    };


    openImage = (index) => {
        this.setState({
            openImageId: index
        })
    };

    closeImage = () => {
        this.setState({
            openImageId: -1
        })
    };

    listImages = () => (
        this.props.images.map(({url}, i) => (
            <ImageMovie key={url} url={url} index={i} openImage={this.openImage}  />)));

    render() {

        const {openImageId} = this.state;

        const listImages = this.listImages();
        const showOpenImage = ~openImageId ? <OpenImage index={openImageId} images={this.props.images}  closeImage={this.closeImage}/> : null;


        return (
            <div>
                <h3 className="title-slider m-b">Кадры из фильма</h3>
                <div className="images-container">
                    {listImages}
                </div>
                {showOpenImage}
            </div>

        );
    }

}

export default SliderImages;