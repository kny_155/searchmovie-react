import React from 'react';

import './style.css'

const Spiner = () => {
    return (
        <div className="place-spiner">
            <div className="lds-css ng-scope">
                <div  className="lds-rolling">
                    <div></div>
                </div>
            </div>
        </div>


    )
};

export default Spiner;