import React from 'react'

import './style.css';

const TitleMovie = ({ props }) => {

    const { name = "", originalName = "" } = props;
    const readyOriginalName = name !== originalName ? originalName : null;

    return (
        <div className="m-b">
            <h2 className="name-movie">{name}</h2>
            <h3 className="text-big">{readyOriginalName}</h3>
        </div>
    );
};

export default TitleMovie

