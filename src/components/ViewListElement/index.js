import React from 'react'

import './style.css'

function ViewListElement(props){
    const styleElement = {
        background: `url(${getPoster()}) no-repeat center`,
        backgroundSize: 'cover'
    };

    function getPoster(){
        if(props.item.poster){
            return `https://image.tmdb.org/t/p/w300${props.item.poster}`;
        }
        return `https://tellyday.com/media/zoo/images/untitled-nikola-tesla-project_bff97b58e9c2b63ff5220ea2e5f2bc49.jpg`;
    }

    return (
        <div className="element-card">
            <div style={styleElement} className="element-card__poster" onClick={() => props.selectedMovie(props.item.id)}>
            </div>
            <div className="element-card__name" onClick={() => props.selectedMovie(props.item.id)}>{props.item.name}</div>
        </div>
    );
}

export default ViewListElement;