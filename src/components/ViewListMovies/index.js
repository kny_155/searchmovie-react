import React, {Component} from 'react';

import MovieService from '../../service/theMovieDbService'

import './style.css';
import ViewListElement from "../ViewListElement";
import Spiner from "../Spiner";

export default class ViewListMovies extends Component{

    constructor(props) {
        super(props);
        this.setNewList(this.props.movieName);
    }

    state = {
        moviesList: [],
        page: 1,
        maxPage: 1,
        maxMovies: -1
    };

    movieService = new MovieService();

    setNewList = (text) => {
        this.getMovieList(text)
            .then(movies => {
                const viewList = this.getViewList(movies);
                this.setState({
                    moviesList: viewList,
                    page: 1,
                    maxPage: movies.totalPages,
                    maxMovies: movies.totalResults
                })
            })
    };

    getNextPage = (text, page) => {

        this.getMovieList(text, page)
            .then(movies => {
                const newMovieList = this.getViewList(movies);
                const moviesList = [...this.state.moviesList, ...newMovieList];
                this.setState({
                    moviesList,
                    page
                })
            })
    };

    getMovieList = (text, page) => {
        return this.movieService.searchMovie(text, page)
    };

    getViewList = (movies) => {
        return movies.results.map((item, i) =>  {
            return <ViewListElement key={item.id + Date.now().toString()} item={item} selectedMovie={this.props.selectedMovie}/>;
        })
    };


    onAddPage = () => {
        if(this.state.page < this.state.maxPage){
            this.getNextPage(this.props.movieName, this.state.page + 1);
        }
    };


    componentDidUpdate(prevProps) {
        if(this.props.movieName !== prevProps.movieName){
            this.setNewList(this.props.movieName);
        }
    }

    render() {
        const {moviesList, page, maxPage, maxMovies} = this.state;
        const button = page !== maxPage ? <button onClick={this.onAddPage} className="add-movies">Еще фильмы</button> : null;
        const error = !maxMovies ? <div>По запросу ничего не найдено</div> : null;
        const loading = !~maxMovies ? <Spiner/> : null;
        return (
            <div className="card-list-cont">
                {error}
                <div className="card-list">
                    {moviesList}
                </div>
                <div >
                    {button}
                </div>
                {loading}
            </div>
        )
    }

}