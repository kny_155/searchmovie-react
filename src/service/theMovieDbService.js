export default class MovieService {

    base = `https://api.themoviedb.org/3`;
    apiKey = `906bffdd2fbfddd0c88efc8b52d39b92`;

    async getResponse(url, attributes) {
        const link = `${this.base}${url}?api_key=${this.apiKey}`;
        const readyLink = attributes ? `${link}&${attributes}` : link;
        const res = await fetch(readyLink);
        if(!res.ok) throw new Error(`Could not Fetch`);
        return await res.json();
    }

    async searchMovie(text, page) {
        const movies = await this.getResponse(`/search/movie`, `language=ru-RU&query=${text}&page=${page}`);
        const movieList = movies.results.map((item) => ({
            id: item.id,
            name: item.title,
            originalName: item["original_title"],
            poster: item["poster_path"],
            rating: item["vote_average"],
            releaseDate: item["release_date"]
        }));
        return ({
            page: movies.page,
            results: movieList,
            totalPages: movies["total_pages"],
            totalResults: movies["total_results"]
        });
    }

    async getMovie(id){
        const movie = await this.getResponse(`/movie/${id}`,`language=ru-RU`);

        const countries = movie["production_countries"].map((country) => country.name);
        const genres = movie.genres.map((genre) => genre.name);
        const data = movie["release_date"].slice(0, 4);
        const images = await this.getImages(id);
        return ({
            id,
            name: movie.title,
            originalName: movie["original_title"],
            countries,
            data,
            genres,
            overview: movie.overview,
            poster: movie["poster_path"],
            budget: movie.budget,
            revenue: movie.revenue,
            release: movie["release_date"],
            runtime: movie.runtime,
            rating: movie["vote_average"],
            images
        })
    }


    async getImages(id){
        const images = await this.getResponse(`/movie/${id}/images`);
        return images.backdrops.map((image) => ({
            url: image['file_path'],
            ratio: image['aspect_ratio']
        }));

    }
}